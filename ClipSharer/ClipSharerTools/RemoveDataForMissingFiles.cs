﻿using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using NLog;

namespace ClipSharerTools
{
    public class RemoveDataForMissingFiles : ITool
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void Use(string collectionDir)
        {
            var clipSharerFolder = Path.Combine(collectionDir, ".ClipSharer");

            var jsons = Directory.EnumerateFiles(clipSharerFolder, "*.json", SearchOption.TopDirectoryOnly);
            foreach(var jf in jsons)
            {
                JObject json = JObject.Parse(File.ReadAllText(jf, Encoding.UTF8));
                JArray filesArray = json.Get<JArray>("files");
                JObject[] files = filesArray.Values<JObject>().ToArray();
                bool changed = false;
                foreach(JObject file in files)
                {
                    string relPath = file.Get<string>("relativePath");
                    string filePath = Path.Combine(collectionDir, relPath);
                    if(!File.Exists(filePath))
                    {
                        logger.Info("Removing data for " + relPath);
                        filesArray.Remove(file);
                        var thumbs = file.Get<JArray>("thumbs");
                        foreach(var thumb in thumbs)
                        {
                            File.Delete(Path.Combine(clipSharerFolder, thumb.Value<string>()));
                        }
                        var thumbName = file.Get<string>("thumbName");
                        File.Delete(Path.Combine(clipSharerFolder, thumbName));
                        changed = true;
                    }
                }
                if(changed)
                {
                    File.WriteAllText(jf, json.ToString(), Encoding.UTF8);
                }
            }
        }
    }
}
