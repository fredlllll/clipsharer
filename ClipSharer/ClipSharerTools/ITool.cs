﻿namespace ClipSharerTools
{
    public interface ITool
    {
        void Use(string collectionDir);
    }
}
