﻿using System.IO;

namespace ClipSharerFFMPEG
{
    public static class Directories
    {
        public static string FFMPEGDir
        {
            get { return Path.Combine(ClipSharerShared.Directories.ProgramDir, "ffmpegbin"); }
        }
    }
}
