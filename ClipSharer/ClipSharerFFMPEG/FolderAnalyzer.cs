﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ClipSharerShared;
using Newtonsoft.Json.Linq;
using NLog;

namespace ClipSharerFFMPEG
{
    public class FolderAnalyzer : RestartableThreadClass
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private readonly DirectoryInfo folder;
        private readonly Options options;
        private readonly string[] fileExtensions;

        public DirectoryInfo ClipSharerFolder { get; }

        public FolderAnalyzer(DirectoryInfo folder, Options options) : base(false)
        {
            this.folder = folder;
            this.options = options;
            ClipSharerFolder = new DirectoryInfo(Path.Combine(folder.FullName, ".ClipSharer"));

            fileExtensions = options.Analyze_FileExtensions.Split(';');
        }

        void CheckClipSharerFolder()
        {
            if(!ClipSharerFolder.Exists)
            {
                logger.Info("Creating clipsharer folder: " + ClipSharerFolder.FullName);
                ClipSharerShared.Directories.CreateDirectorySync(ClipSharerFolder.FullName);
            }
            ClipSharerFolder.Refresh();
            ClipSharerFolder.Attributes |= FileAttributes.Hidden;
        }

        IEnumerable<string> GetAllFiles()
        {
            IEnumerable<string> retval = null;
            foreach(var ext in fileExtensions)
            {
                var files = Directory.EnumerateFiles(folder.FullName, "*." + ext, SearchOption.AllDirectories);
                if(retval == null)
                {
                    retval = files;
                }
                else
                {
                    retval = retval.Concat(files);
                }
            }
            return retval;
        }

        JObject GetInfo(string infoFile)
        {
            if(File.Exists(infoFile))
            {
                return JObject.Parse(File.ReadAllText(infoFile, Encoding.UTF8));
            }
            return new JObject
            {
                ["files"] = new JArray()
            };
        }

        /// <summary>
        /// gets a file info object from the info object. returns true if one was found, or false if a new one was created
        /// </summary>
        /// <param name="info"></param>
        /// <param name="file"></param>
        /// <param name="outFileInfo"></param>
        /// <returns></returns>
        bool GetFileInfo(JObject info, string file, out JObject outFileInfo)
        {
            JArray files = (JArray)info["files"];
            string relativePath = Files.GetRelativePath(file, folder.FullName);
            foreach(JObject fileInfo in files)
            {
                if(fileInfo.Get<string>("relativePath").Equals(relativePath))
                {
                    outFileInfo = fileInfo;
                    return true;
                }
            }
            outFileInfo = new JObject();
            files.Add(outFileInfo);
            return false;
        }

        /// <summary>
        /// if the file is greater than <paramref name="hashLength"/> bytes, this will generate one hash from the start and one from the end of the file.
        /// the hashes are then concatenated and are hashed again to get an indicative md5 hash for the file. if the file is smaller than hashLength there will be only one hash which will be returned
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        string GetHash(string file, long hashLength = 10000000)
        {
            using(MD5Cng md5 = new MD5Cng())
            {
                FileInfo fi = new FileInfo(file);
                byte[] hashBytes;
                using(FileStreamSlice fs = new FileStreamSlice(file, FileMode.Open, FileAccess.Read, FileShare.Read, 0, hashLength)) //hash first x bytes
                {
                    hashBytes = md5.ComputeHash(fs);
                }
                if(fi.Length > hashLength)
                {
                    using(FileStreamSlice fs = new FileStreamSlice(file, FileMode.Open, FileAccess.Read, FileShare.Read, fi.Length - hashLength, hashLength)) //hash last x bytes
                    {
                        hashBytes = hashBytes.Concat(md5.ComputeHash(fs)).ToArray();
                    }
                }
                if(hashBytes.Length > 16)
                {
                    hashBytes = md5.ComputeHash(hashBytes); //cause hashBytes can have differing lengths
                }
                return hashBytes.ToHex();
            }
        }

        protected override void Run()
        {
            try
            {
                logger.Info("Analyzing folder: " + folder.FullName);
                Stopwatch sw = new Stopwatch();
                sw.Start();

                CheckClipSharerFolder();
                var allFiles = GetAllFiles();
                Parallel.ForEach(allFiles, (file) =>
                {
                    //foreach(var file in allFiles)
                    //{
                    if(!file.StartsWith(ClipSharerFolder.FullName)) //ignore files in clipsharer folder if they slipped in by accident
                    {
                        AnalyzeFile(ClipSharerFolder, file);
                    }
                    Thread.Sleep(0);
                });
                sw.Stop();
                logger.Info("Analyzing done in " + sw.Elapsed);
            }
            catch(ThreadInterruptedException)
            {
                logger.Info("Folder Analyzer got interrupted");
                //end
            }
        }

        void AnalyzeFile(DirectoryInfo clipSharerFolder, string file)
        {
            logger.Info("Analyzing " + file);

            string hash = GetHash(file);
            logger.Info("Hash: " + hash);

            string infoFile = Path.Combine(clipSharerFolder.FullName, hash + ".json");
            JObject info = GetInfo(infoFile);
            if(!GetFileInfo(info, file, out JObject fileInfo) || options.Analyze_OverwriteAnalyticData) //not processed yet, or overwrite
            {
                PopulateFFProbeInfo(fileInfo, file);
                File.WriteAllText(infoFile, info.ToString(), Encoding.UTF8);
            }
        }

        void PopulateFFProbeInfo(JObject fileInfo, string file)
        {
            string relativePath = Files.GetRelativePath(file, folder.FullName);
            fileInfo["relativePath"] = relativePath;

            logger.Info("Extracting ffprobe info for " + relativePath);
            var probeInfo = FFMPEG.FFProbe(file);
            fileInfo["ffProbe"] = probeInfo.ffProbeOutput;
        }
    }
}
