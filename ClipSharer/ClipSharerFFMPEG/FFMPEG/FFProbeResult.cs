﻿using Newtonsoft.Json.Linq;

namespace ClipSharerFFMPEG
{
    public class FFProbeResult
    {
        public readonly FFProbeFormatInfo format;
        public readonly FFProbeStreamInfo[] streams;
        public readonly JObject ffProbeOutput;

        public FFProbeResult(JObject ffProbeOutput)
        {
            JArray streams = ffProbeOutput.Get<JArray>("streams");
            this.streams = new FFProbeStreamInfo[streams.Count];
            for(int i = 0; i < streams.Count; i++)
            {
                JObject stream = (JObject)streams[i];
                this.streams[i] = new FFProbeStreamInfo(stream);
            }
            this.format = new FFProbeFormatInfo(ffProbeOutput.Get<JObject>("format"));
            this.ffProbeOutput = ffProbeOutput;
        }
    }
}
