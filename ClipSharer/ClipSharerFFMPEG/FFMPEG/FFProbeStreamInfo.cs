﻿using Newtonsoft.Json.Linq;

namespace ClipSharerFFMPEG
{
    public class FFProbeStreamInfo
    {
        public readonly JObject info;

        public readonly int index;
        public readonly string codec_name;
        public readonly string codec_long_name;
        public readonly string codec_type;
        public readonly double duration;

        public FFProbeStreamInfo(JObject info)
        {
            this.info = info;
            index = Get<int>("index");
            codec_name = Get<string>("codec_name");
            codec_long_name = Get<string>("codec_long_name");
            codec_type = Get<string>("codec_type");
            duration = Get<double>("duration");
        }

        public T Get<T>(string key, T def = default(T))
        {
            return info.Get<T>(key, def);
        }
    }
}
