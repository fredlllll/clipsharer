﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ClipSharerFFMPEG
{
    /// <summary>
    /// when disposed or finalized this will delete the generated frames and other byproducts that belong to it
    /// </summary>
    public class FFMPEGFrameReductionResult : IDisposable
    {
        private readonly string framesFolder;

        public List<string> Frames { get; }

        public FFMPEGFrameReductionResult(List<string> frames, string framesFolder)
        {
            Frames = frames;
            this.framesFolder = framesFolder;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if(!disposedValue)
            {
                if(disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                if(Directory.Exists(framesFolder))
                {
                    Directory.Delete(framesFolder, true);
                }

                disposedValue = true;
            }
        }

        ~FFMPEGFrameReductionResult()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion


    }
}
