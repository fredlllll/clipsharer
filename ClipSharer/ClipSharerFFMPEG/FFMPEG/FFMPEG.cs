﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using ClipSharerShared;
using Newtonsoft.Json.Linq;

namespace ClipSharerFFMPEG
{
    public static class FFMPEG
    {
        static long analyzeDuration = 1000000;

        public static JObject FFProbeJSON(string file)
        {
            string ffProbePath = Path.Combine(ClipSharerFFMPEG.Directories.FFMPEGDir, "ffprobe.exe");

            using(Process p = ProcessUtil.CreateProcess(ffProbePath, "-analyzeduration " + analyzeDuration + " -loglevel quiet -print_format json -show_streams -show_format " + ProcessUtil.EscapeArgument(file), redirectStdOut: true))
            {
                p.Start();
                p.PriorityClass = ProcessPriorityClass.BelowNormal;
                using(StreamReader myReader = new StreamReader(p.StandardOutput.BaseStream, Encoding.UTF8))
                {
                    return JObject.Parse(myReader.ReadToEnd());
                }
            }
        }

        public static FFProbeResult FFProbe(string file)
        {
            return new FFProbeResult(FFProbeJSON(file));
        }

        /// <summary>
        /// this empties the tmpthumbsdir before executing
        /// extracts some frames from the video. ideally the number of frames will be the same as specified in count.
        /// Some videos are broken though and yield more or less frames. the actual list of frames is returned
        /// </summary>
        /// <param name="file"></param>
        /// <param name="duration"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static FFMPEGFrameReductionResult ReduceToFrames(string file, double duration, int count)
        {
            string ffMpegPath = Path.Combine(ClipSharerFFMPEG.Directories.FFMPEGDir, "ffmpeg.exe");
            string randomID = Util.GetRandomString();
            string tmpFolder = Path.Combine(ClipSharerShared.Directories.ProgramDir, "tmpthumbs", randomID);
            Directory.CreateDirectory(tmpFolder);
            while(!Directory.Exists(tmpFolder))
            {
                Thread.Sleep(50);
            }

            List<string> frames = new List<string>();
            double timeOffset = (1.0 / (count + 1)) * duration;
            List<Process> processes = new List<Process>();
            for(int i = 0; i < count; i++)
            {
                double offset = (i + 1) * timeOffset;
                string frameName = Path.Combine(tmpFolder, randomID + i.ToString("D3") + ".jpg");
                string args = "-ss " + offset.ToString(CultureInfo.InvariantCulture) + " -i " + ProcessUtil.EscapeArgument(file) + " -vframes 1 -q:v 5 " + frameName;
                Process p = ProcessUtil.CreateProcess(ffMpegPath, args, createNoWindow: true);
                p.Start();
                p.PriorityClass = ProcessPriorityClass.BelowNormal;
                processes.Add(p);
                frames.Add(frameName);
            }

            foreach(var p in processes)
            {
                p.WaitForExit();
                p.Dispose();
            }

            return new FFMPEGFrameReductionResult(frames, tmpFolder);
        }
    }
}
