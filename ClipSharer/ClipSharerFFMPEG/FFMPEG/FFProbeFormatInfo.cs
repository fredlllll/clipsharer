﻿using Newtonsoft.Json.Linq;

namespace ClipSharerFFMPEG
{
    public class FFProbeFormatInfo
    {
        public readonly JObject info;

        public readonly string filename;
        public readonly string format_name;
        public readonly string format_long_name;
        public readonly double duration;
        public readonly long size;
        public readonly long bit_rate;

        public FFProbeFormatInfo(JObject info)
        {
            this.info = info;

            filename = Get<string>("filename");
            format_name = Get<string>("format_name");
            format_long_name = Get<string>("format_long_name");
            duration = Get<double>("duration");
            size = Get<long>("size");
            bit_rate = Get<long>("bit_rate");
        }

        public T Get<T>(string key, T def = default(T))
        {
            return info.Get<T>(key, def);
        }
    }
}
