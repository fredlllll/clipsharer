﻿using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using ClipSharerShared;
using Newtonsoft.Json.Linq;
using NLog;

namespace ClipSharerFFMPEG
{
    public class FrameExtractor : RestartableThreadClass
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly DirectoryInfo clipSharerDirectory;
        private readonly Options options;

        public FrameExtractor(DirectoryInfo clipSharerDirectory, Options options) : base(false)
        {
            this.clipSharerDirectory = clipSharerDirectory;
            this.options = options;
        }

        JArray GetThumbsArray(JObject fileInfo)
        {
            JArray retval = (JArray)fileInfo["thumbs"];
            if(retval.IsNull())
            {
                retval = new JArray();
                fileInfo["thumbs"] = retval;
            }
            return retval;
        }

        void ProcessFile(JObject fileInfo)
        {
            logger.Info("Checking if frames exist for " + fileInfo.Get<string>("relativePath"));

            FFProbeResult probeInfo = new FFProbeResult((JObject)fileInfo["ffProbe"]);

            JArray thumbsArray = GetThumbsArray(fileInfo);
            if(thumbsArray.Count <= 0 || options.Extract_ReextractFrames)
            {
                if(thumbsArray.Count > 0) //delete old frames
                {
                    logger.Info("Deleting old Frames");
                    foreach(var t in thumbsArray)
                    {
                        File.Delete(Path.Combine(clipSharerDirectory.FullName, t.Value<string>()));
                    }
                    thumbsArray.Clear();
                }

                logger.Info("Extracting frames from " + fileInfo.Get<string>("relativePath"));
                var thumbs = FFMPEG.ReduceToFrames(probeInfo.format.filename, probeInfo.format.duration, options.Thumbnails_GridHeight * options.Thumbnails_GridWidth);

                foreach(var frame in thumbs.Frames)
                {
                    string fname = Path.GetFileName(frame);
                    thumbsArray.Add(fname);
                    try
                    {
                        File.Move(frame, Path.Combine(clipSharerDirectory.FullName, fname));
                    }
                    catch(IOException)
                    {
                        logger.Warn("error when moving file " + fname);
                    }
                }
            }
        }

        protected override void Run()
        {
            try
            {
                logger.Info("Extracting frames");
                Stopwatch sw = new Stopwatch();
                sw.Start();

                var jsonFiles = Directory.EnumerateFiles(clipSharerDirectory.FullName, "*.json", SearchOption.TopDirectoryOnly);
                foreach(var jf in jsonFiles)
                {
                    JObject info = JObject.Parse(File.ReadAllText(jf, Encoding.UTF8));
                    JArray files = (JArray)info["files"];
                    foreach(JObject f in files)
                    {
                        ProcessFile(f);
                    }
                    File.WriteAllText(jf, info.ToString(), Encoding.UTF8);
                    Thread.Sleep(0);
                }

                logger.Info("Frames extracted in " + sw.Elapsed);
            }
            catch(ThreadInterruptedException)
            {
                logger.Info("Frame Extractor got interrupted");
                //end
            }
        }
    }
}
