﻿using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace ClipSharerShared
{
    public class Options
    {
        [Category("Steps")]
        [DisplayName("Analyze")]
        public bool Analyze { get; set; } = true;
        [Category("Steps")]
        [DisplayName("Extract Frames")]
        public bool ExtractFrames { get; set; } = true;
        [Category("Steps")]
        [DisplayName("Generate Thumbnails")]
        public bool GenerateThumbnails { get; set; } = true;
        [Category("Steps")]
        [DisplayName("Generate Website")]
        public bool GenerateWebsite { get; set; } = true;

        [Category("Analyze")]
        [DisplayName("Overwrite Analytic Data")]
        [Description("Determines if the file is analyzed again, even if it already has been analyzed")]
        public bool Analyze_OverwriteAnalyticData { get; set; } = false;

        [Category("Analyze")]
        [DisplayName("File Extensions")]
        [Description("Only files with these extensions will be analyzed")]
        public string Analyze_FileExtensions { get; set; } = "mp4;avi;wmv;rm;m4v";

        [Category("Extract Frames")]
        [DisplayName("Reextract Frames")]
        [Description("Determines if the frames are extracted again if they already exist")]
        public bool Extract_ReextractFrames { get; set; } = false;

        [Category("Generate Thumbnails")]
        [DisplayName("Overwrite Thumbnails")]
        [Description("Determines if the thumbnails are generated again if they already exist")]
        public bool Thumbnails_OverwriteThumbnails { get; set; } = false;

        [Category("Generate Thumbnails")]
        [DisplayName("Thumbnail Width")]
        [Description("The width of the resulting thumbnail image in pixels")]
        public int Thumbnails_ThumbWidth { get; set; } = 1280;

        [Category("Generate Thumbnails")]
        [DisplayName("Grid Width")]
        public int Thumbnails_GridWidth { get; set; } = 4;

        [Category("Generate Thumbnails")]
        [DisplayName("Grid Height")]
        public int Thumbnails_GridHeight { get; set; } = 4;

        [Category("Generate Thumbnails")]
        [DisplayName("Thumbnails Spacing")]
        public int Thumbnails_ThumbSpacing { get; set; } = 5;

        [Category("Generate Thumbnails")]
        [DisplayName("Thumbnail Background")]
        public Color Thumbnails_ThumbBackground { get; set; } = Color.DarkGray;

        [Category("Generate Thumbnails")]
        [DisplayName("Thumbnail Font")]
        public Font Thumbnails_Font { get; set; } = SystemFonts.DefaultFont;

        [Category("Generate Website")]
        [DisplayName("Overwrite Thumbnails")]
        [Description("Determines if the thumbnails are copied to the website folders again if they already exist")]
        public bool Website_OverwriteThumbnails { get; set; } = false;

        public void Load(string file)
        {
            if(File.Exists(file))
            {
                JsonConvert.PopulateObject(File.ReadAllText(file, Encoding.UTF8), this);
            }
        }

        public void Save(string file)
        {
            File.WriteAllText(file, JsonConvert.SerializeObject(this), Encoding.UTF8);
        }
    }
}
