﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;

namespace ClipSharerShared
{
    public static class Directories
    {
        /// <summary>
        /// Gives the directory that contains the assembly that called this
        /// </summary>
        public static string AssemblyDir
        {
            get
            {
                return Path.GetDirectoryName(Assembly.GetCallingAssembly().Location);
            }
        }

        /// <summary>
        /// Gives the directory that contains the entry assembly
        /// </summary>
        public static string ProgramDir
        {
            get
            {
                return Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            }
        }

        /// <summary>
        /// Gives the users (windows) or systems (linux) temp directory
        /// </summary>
        public static string TempDir
        {
            get
            {
                return Path.GetTempPath();
            }
        }

        /// <summary>
        /// creats all directories in the path, just like Directory.CreateDirectory. it additionally waits till the directory was really created by the filesystem.
        /// throws IOException on timeout
        /// </summary>
        /// <param name="path"></param>
        /// <param name="timeout"></param>
        /// <exception cref="IOException">if directory isnt created within timeout</exception>
        /// <returns></returns>
        public static DirectoryInfo CreateDirectorySync(string path, int timeout = 5000)
        {
            DirectoryInfo retval = Directory.CreateDirectory(path);
            DateTime start = DateTime.UtcNow;
            retval.Refresh();
            while(!retval.Exists)
            {
                var diff = DateTime.UtcNow - start;
                if(diff.TotalMilliseconds > timeout)
                {
                    throw new IOException("Directory wasnt created within timeout (" + timeout + " ms) using Directory.CreateDirectory");
                }
                Thread.Sleep(50);
                retval.Refresh();
            }
            return retval;
        }
    }
}
