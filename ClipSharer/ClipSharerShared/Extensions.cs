﻿using System.Globalization;
using System.Text;

public static class Extensions
{
    public static string FirstCharacterToLower(this string str)
    {
        if(string.IsNullOrEmpty(str) || char.IsLower(str, 0))
        {
            return str;
        }

        return char.ToLowerInvariant(str[0]) + str.Substring(1);
    }

    public static string FirstCharacterToUpper(this string str)
    {
        if(string.IsNullOrEmpty(str) || char.IsUpper(str, 0))
        {
            return str;
        }

        return char.ToUpperInvariant(str[0]) + str.Substring(1);
    }

    public static string ToHex(this byte[] bytes, bool upperCase = false)
    {
        StringBuilder result = new StringBuilder(bytes.Length * 2);

        string format = upperCase ? "X2" : "x2";
        for(int i = 0; i < bytes.Length; i++)
        {
            result.Append(bytes[i].ToString(format));
        }

        return result.ToString();
    }

    public static string FormatFileSize(this long val)
    {
        if(val < 1000) //to prevent decimal point
        {
            return val + "B";
        }
        string[] sizes = new string[] { "B", "KB", "MB", "GB", "TB", "PB", "EB" };
        double dval = val;
        int i = 0;
        while(dval >= 1024 && i < sizes.Length - 1)
        {
            i++;
            dval /= 1024;
        }
        return string.Format(CultureInfo.InvariantCulture, "{0:0.##} {1}", dval, sizes[i]);
    }

}