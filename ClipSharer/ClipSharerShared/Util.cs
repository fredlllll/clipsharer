﻿using System;
using System.Globalization;

namespace ClipSharerShared
{
    public static class Util
    {
        public static Random R { get; } = new Random();

        public static string GetRandomString(int length = 20)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[length];

            for(int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[R.Next(chars.Length)];
            }

            return new String(stringChars);
        }

        public static double ParseRational(string rational)
        {
            string[] parts = rational.Split('/');
            double a = double.Parse(parts[0], CultureInfo.InvariantCulture);
            if(parts.Length == 1)
            {
                return a;
            }
            double b = double.Parse(parts[1], CultureInfo.InvariantCulture);
            return a / b;
        }
    }
}
