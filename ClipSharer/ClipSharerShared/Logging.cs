﻿using NLog;
using NLog.Config;
using NLog.Targets;

namespace ClipSharerShared
{
    public static class Logging
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private static LoggingConfiguration lc = new LoggingConfiguration();

        public static void SetupLogging()
        {
            AddConsole("console");

            logger.Info("Logging setup done");
        }

        public static void AddConsole(string name)
        {
            var consoleTarget = new ColoredConsoleTarget
            {
                Layout = @"[${date}][${logger}]: ${message}"
            };

            AddTarget(consoleTarget, name);
        }

        public static void AddLogFile(string name, string file)
        {
            var fileTarget = new FileTarget
            {
                FileName = file,
                Layout = @"[${date}][${logger}]: ${message}"
            };

            AddTarget(fileTarget, name);
        }

        public static void AddTarget(Target target, string name = "")
        {
            lc.AddTarget(name, target);

            var rule = new LoggingRule("*", LogLevel.Debug, target);

            lc.LoggingRules.Add(rule);

            LogManager.Configuration = lc;

        }
    }
}
