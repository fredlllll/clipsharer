﻿using System.IO;

namespace ClipSharerShared
{
    public class FileStreamSlice : FileStream
    {
        public FileStreamSlice(string path, FileMode mode, FileAccess access, FileShare share, long sliceOffset, long sliceEnd) : base(path, mode, access, share)
        {
            base.Seek(sliceOffset, SeekOrigin.Begin);
            SliceEnd = sliceEnd;
        }

        public long SliceEnd { get; set; }

        public override int Read(byte[] array, int offset, int count)
        {
            if(base.Position >= this.SliceEnd)
            {
                return 0;
            }

            if(base.Position + count > this.SliceEnd)
            {
                count = (int)(this.SliceEnd - base.Position);
            }

            return base.Read(array, offset, count);
        }
    }
}
