﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ClipSharerFFMPEG;
using ClipSharerShared;
using Newtonsoft.Json.Linq;
using NLog;

namespace ClipSharerThumbnail
{
    public class ThumbnailGenerator : RestartableThreadClass
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private readonly DirectoryInfo clipSharerDirectory;
        private readonly Options options;

        Brush BackgroundBrush { get; }

        public ThumbnailGenerator(DirectoryInfo clipSharerDirectory, Options options) : base(false)
        {
            this.clipSharerDirectory = clipSharerDirectory;
            this.options = options;

            BackgroundBrush = new SolidBrush(options.Thumbnails_ThumbBackground);
        }

        JObject GetInfo(string infoFile)
        {
            if(File.Exists(infoFile))
            {
                return JObject.Parse(File.ReadAllText(infoFile, Encoding.UTF8));
            }
            return new JObject
            {
                ["files"] = new JArray()
            };
        }

        void GenerateThumbail(JObject fileInfo)
        {
            if(options.Thumbnails_OverwriteThumbnails)
            {
                if(!fileInfo["thumbName"].IsNullOrEmpty())
                {
                    logger.Info("Deleting old thumbnail");
                    File.Delete(Path.Combine(clipSharerDirectory.FullName, fileInfo.Get<string>("thumbName")));
                }
            }

            logger.Info("Generating Thumbnail for " + fileInfo.Get<string>("relativePath"));
            using(Bitmap header = GenerateThumbHeader(fileInfo))
            using(Bitmap body = GenerateThumbBody(fileInfo))
            using(Bitmap target = new Bitmap(header.Width, header.Height + body.Height))
            {
                using(Graphics g = Graphics.FromImage(target))
                {
                    g.DrawImageUnscaled(header, 0, 0);
                    g.DrawImageUnscaled(body, 0, header.Height);
                    g.Flush();
                }
                string thumbName;
                if(!fileInfo["thumbName"].IsNullOrEmpty())
                {
                    thumbName = fileInfo.Get<string>("thumbName");
                }
                else
                {
                    thumbName = Util.GetRandomString() + ".jpg";
                    fileInfo["thumbName"] = thumbName;
                }
                string thumbPath = Path.Combine(clipSharerDirectory.FullName, thumbName);
                target.Save(thumbPath, ImageFormat.Jpeg);
            }
        }

        Bitmap GenerateThumbHeader(JObject fileInfo)
        {
            FFProbeResult probe = new FFProbeResult((JObject)fileInfo["ffProbe"]);
            FFProbeStreamInfo videoStream = probe.streams.Where((x) => x.codec_type.Equals("video")).First();

            Dictionary<string, string> values = new Dictionary<string, string>()
            {
                ["File Name"] = Path.GetFileName(probe.format.filename),
                ["File Size"] = probe.format.size.FormatFileSize(),
                ["Resolution"] = videoStream.Get<string>("width") + "x" + videoStream.Get<string>("height"),
                ["FPS"] = Util.ParseRational(videoStream.Get<string>("r_frame_rate")).ToString("F2", CultureInfo.InvariantCulture),
                ["Duration"] = TimeSpan.FromSeconds(probe.format.duration).ToString(@"hh\:mm\:ss"),
            };

            Font headerFont = options.Thumbnails_Font;
            Brush textBrush = Brushes.Black;

            Bitmap target = new Bitmap(options.Thumbnails_ThumbWidth, options.Thumbnails_ThumbSpacing + headerFont.Height * values.Count);

            using(Graphics g = Graphics.FromImage(target))
            {
                lock(BackgroundBrush)
                {
                    g.FillRectangle(BackgroundBrush, 0, 0, target.Width, target.Height);
                }

                int offsetTop = options.Thumbnails_ThumbSpacing;
                foreach(var kv in values)
                {
                    g.DrawString(kv.Key + ": " + kv.Value, headerFont, textBrush, options.Thumbnails_ThumbSpacing, offsetTop);
                    offsetTop += headerFont.Height;
                }

                g.Flush();
            }

            return target;
        }

        Bitmap GenerateThumbBody(JObject fileInfo)
        {
            var thumbs = (JArray)fileInfo["thumbs"];
            string firstThumbFile = Path.Combine(clipSharerDirectory.FullName, thumbs[0].Value<string>());
            if(!File.Exists(firstThumbFile))
            {
                Bitmap errorImage = new Bitmap(options.Thumbnails_ThumbWidth, 20);
                using(Graphics errorGraphics = Graphics.FromImage(errorImage))
                {
                    lock(BackgroundBrush)
                    {
                        errorGraphics.FillRectangle(BackgroundBrush, 0, 0, errorImage.Width, errorImage.Height);
                    }
                    errorGraphics.DrawString("could not find thumbnails for this image. maybe they were not properly generated", options.Thumbnails_Font, Brushes.Black, 3, 3);
                    errorGraphics.Flush();
                }
                return errorImage;
            }
            Bitmap firstThumb = new Bitmap(firstThumbFile);

            int thumbWidth = options.Thumbnails_ThumbWidth;
            int gridWidth = options.Thumbnails_GridWidth;
            int gridHeight = options.Thumbnails_GridHeight;
            int thumbSpacing = options.Thumbnails_ThumbSpacing;

            int thumbHeight = (int)((firstThumb.Height / (double)firstThumb.Width) * thumbWidth);
            int mThumbWidth = (thumbWidth - (gridWidth + 1) * thumbSpacing) / gridWidth;
            int mThumbHeight = (thumbHeight - (gridHeight + 1) * thumbSpacing) / gridHeight;
            firstThumb.Dispose();

            Bitmap target = new Bitmap(thumbWidth, thumbHeight);
            using(Graphics targetG = Graphics.FromImage(target))
            {
                lock(BackgroundBrush)
                {
                    targetG.FillRectangle(BackgroundBrush, 0, 0, target.Width, target.Height);
                }
                for(int y = 0; y < gridHeight; y++)
                {
                    for(int x = 0; x < gridWidth; x++)
                    {
                        int index = y * gridWidth + x;
                        int offsetX = thumbSpacing + x * thumbSpacing + x * mThumbWidth;
                        int offsetY = thumbSpacing + y * thumbSpacing + y * mThumbHeight;

                        string thumbFile = Path.Combine(clipSharerDirectory.FullName, thumbs[index].Value<string>());
                        if(File.Exists(thumbFile))
                        {
                            Bitmap thumb = new Bitmap(thumbFile);
                            targetG.DrawImage(thumb, new Rectangle(offsetX, offsetY, mThumbWidth, mThumbHeight));
                            thumb.Dispose();
                        }
                        else
                        {
                            logger.Warn("Thumb " + thumbs[index].Value<string>() + " couldnt be found, it was probably not generated");
                        }
                    }
                }
                targetG.Flush();

                return target;
            }
        }

        protected override void Run()
        {
            try
            {
                logger.Info("Generating thumbnails");
                Stopwatch sw = new Stopwatch();
                sw.Start();

                var jsonFiles = Directory.EnumerateFiles(clipSharerDirectory.FullName, "*.json", SearchOption.TopDirectoryOnly);
                Parallel.ForEach(jsonFiles, (jf) =>
                {
                    JObject info = JObject.Parse(File.ReadAllText(jf, Encoding.UTF8));
                    JArray files = (JArray)info["files"];
                    foreach(JObject f in files)
                    {
                        string thumbName = f.Get<string>("thumbName");
                        bool generate = true;
                        if(!string.IsNullOrWhiteSpace(thumbName) && File.Exists(Path.Combine(clipSharerDirectory.FullName, thumbName)))
                        {
                            logger.Info("Thumbnail already exists for " + f.Get<string>("relativePath"));
                            generate = false;
                        }
                        if(generate || options.Thumbnails_OverwriteThumbnails)
                        {
                            GenerateThumbail(f);
                        }
                    }
                    File.WriteAllText(jf, info.ToString(), Encoding.UTF8);
                    Thread.Sleep(0);
                });

                logger.Info("Thumbnails generated in " + sw.Elapsed);
            }
            catch(ThreadInterruptedException)
            {
                logger.Info("Thumbnail Generator got interrupted");
                //end
            }
        }
    }
}
