﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using ClipSharerShared;
using Newtonsoft.Json.Linq;
using NLog;

namespace ClipSharerWebsite
{
    public class WebsiteGenerator : RestartableThreadClass
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private readonly DirectoryInfo clipSharerDirectory;
        private readonly DirectoryInfo websiteTargetDir;
        private readonly Options options;

        public WebsiteGenerator(DirectoryInfo clipSharerDirectory, DirectoryInfo websiteTargetDir, Options options) : base(false)
        {
            this.clipSharerDirectory = clipSharerDirectory;
            this.websiteTargetDir = websiteTargetDir;
            this.options = options;
        }

        VirtualDirectory GenerateVirtualTree()
        {
            logger.Info("Generating virtual folder structure");

            VirtualDirectory rootDir = new VirtualDirectory(null, "root");

            var jsonFiles = Directory.EnumerateFiles(clipSharerDirectory.FullName, "*.json", SearchOption.TopDirectoryOnly);
            foreach(var jf in jsonFiles)
            {
                JObject info = JObject.Parse(File.ReadAllText(jf, Encoding.UTF8));
                JArray files = (JArray)info["files"];
                foreach(JObject f in files)
                {
                    string relativePath = f.Get<string>("relativePath");

                    string relativeDirectory = Path.GetDirectoryName(relativePath);
                    VirtualDirectory dir = rootDir;
                    if(relativeDirectory.Length > 0)
                    {
                        string[] dirs = relativeDirectory.Split('\\');
                        foreach(var dirName in dirs)
                        {
                            dir = dir.GetDirectory(dirName);
                        }
                    }

                    VirtualFile file = dir.CreateFile(Path.GetFileName(relativePath), f);
                }
                File.WriteAllText(jf, info.ToString(), Encoding.UTF8);
            }

            return rootDir;
        }

        /// <summary>
        /// creates a virtual directory in targetDir
        /// </summary>
        /// <param name="targetDir">the directory tha virtual directory should be put in</param>
        /// <param name="dir">the virtual directory</param>
        void GenerateThumbTree(DirectoryInfo targetDir, VirtualDirectory dir)
        {
            ClipSharerShared.Directories.CreateDirectorySync(targetDir.FullName);
            foreach(var subDir in dir.directories)
            {
                GenerateThumbTree(new DirectoryInfo(Path.Combine(targetDir.FullName, subDir.Key)), subDir.Value);
                Thread.Sleep(0);
            }
            foreach(var thumb in dir.files)
            {
                string thumbName = thumb.Value.fileInfo.Get<string>("thumbName");
                string dst = Path.Combine(targetDir.FullName, thumbName);
                if(!File.Exists(dst) || options.Website_OverwriteThumbnails)
                {
                    string src = Path.Combine(clipSharerDirectory.FullName, thumbName);
                    File.Copy(src, dst, true);
                    ThumbUtil.CreateSmallThumb(dst); //creates smaller thumb next to it with prefix
                }
                Thread.Sleep(0);
            }
        }

        void GenerateThumbIndices(DirectoryInfo targetDir, VirtualDirectory dir)
        {
            ClipSharerShared.Directories.CreateDirectorySync(targetDir.FullName);
            Website site = new Website();
            List<ItemInfo> items = new List<ItemInfo>();
            string dirPath = dir.GetPath();
            //using C:/ as prefix cause relative paths dont work when given relative paths
            string relFolderIcon = Files.GetRelativePath("C:/root/folder.png", "C:/" + dirPath);
            string relFolderUpIcon = Files.GetRelativePath("C:/root/folder_up.png", "C:/" + dirPath);

            if(dir.parent != null)
            {
                items.Add(new ItemInfo("..", relFolderUpIcon, "../index.html"));
            }

            foreach(var subDir in dir.directories)
            {
                GenerateThumbIndices(new DirectoryInfo(Path.Combine(targetDir.FullName, subDir.Key)), subDir.Value);
                items.Add(new ItemInfo(WebUtility.HtmlEncode(subDir.Key), relFolderIcon, Uri.EscapeDataString(subDir.Key) + "/index.html"));
                Thread.Sleep(0);
            }
            foreach(var thumb in dir.files)
            {
                string thumbName = thumb.Value.fileInfo.Get<string>("thumbName");
                string fileName = WebUtility.HtmlEncode(Path.GetFileName(thumb.Value.fileInfo.Get<string>("relativePath")));
                items.Add(new ItemInfo(fileName, "thumb_" + thumbName, thumbName));
                Thread.Sleep(0);
            }
            File.WriteAllText(Path.Combine(targetDir.FullName, "index.html"), site.Render(dir.name, items), Encoding.UTF8);
        }

        protected override void Run()
        {
            try
            {
                logger.Info("Generating website in " + websiteTargetDir.FullName);
                Stopwatch sw = new Stopwatch();
                sw.Start();

                VirtualDirectory rootDir = GenerateVirtualTree();

                ClipSharerShared.Directories.CreateDirectorySync(websiteTargetDir.FullName);

                List<string> additionalFiles = new List<string>()
                {
                    "folder.png",
                    "folder_up.png"
                };
                foreach(var f in additionalFiles)
                {
                    File.Copy(Path.Combine(Directories.TemplatesDir, f), Path.Combine(websiteTargetDir.FullName, f), true);
                }

                logger.Info("Adding thumbs to website folder");
                GenerateThumbTree(websiteTargetDir, rootDir);
                logger.Info("Generating index files");
                GenerateThumbIndices(websiteTargetDir, rootDir);

                logger.Info("Website generated in " + sw.Elapsed);
            }
            catch(ThreadInterruptedException)
            {
                logger.Info("Website Generator got interrupted");
                //end
            }
        }
    }
}
