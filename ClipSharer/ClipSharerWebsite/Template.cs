﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ClipSharerWebsite
{
    public class Template
    {
        string templateContent;

        public Template(string name)
        {
            templateContent = File.ReadAllText(Path.Combine(Directories.TemplatesDir, name + ".tpl.html"), Encoding.UTF8);
        }

        public string Render(List<Tuple<string, string>> values)
        {
            string tmp = templateContent;
            foreach(var val in values)
            {
                tmp = tmp.Replace("%%" + val.Item1 + "%%", val.Item2);
            }
            return tmp;
        }
    }
}
