﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace ClipSharerWebsite
{
    public class VirtualDirectory
    {
        public readonly VirtualDirectory parent;
        public readonly string name;
        public readonly SortedDictionary<string, VirtualFile> files = new SortedDictionary<string, VirtualFile>();
        public readonly SortedDictionary<string, VirtualDirectory> directories = new SortedDictionary<string, VirtualDirectory>();

        public VirtualDirectory(VirtualDirectory parent, string name)
        {
            this.parent = parent;
            this.name = name;
        }

        public VirtualDirectory GetDirectory(string name)
        {
            if(!directories.TryGetValue(name, out VirtualDirectory retval))
            {
                retval = new VirtualDirectory(this, name);
                directories[name] = retval;
            }
            return retval;
        }

        public VirtualFile CreateFile(string name, JObject fileInfo)
        {
            VirtualFile file = new VirtualFile(this, name, fileInfo);
            files[name] = file;
            return file;
        }

        public string GetPath()
        {
            List<string> parts = new List<string>();
            VirtualDirectory here = this;
            while(here != null)
            {
                parts.Add(here.name);
                here = here.parent;
            }
            parts.Reverse();
            return string.Join("/", parts);
        }
    }
}
