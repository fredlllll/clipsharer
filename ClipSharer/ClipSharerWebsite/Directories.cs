﻿using System.IO;

namespace ClipSharerWebsite
{
    public static class Directories
    {
        public static string TemplatesDir
        {
            get { return Path.Combine(ClipSharerShared.Directories.ProgramDir,"websiteTemplates"); }
        }
    }
}
