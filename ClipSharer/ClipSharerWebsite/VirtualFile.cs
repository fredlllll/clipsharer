﻿using Newtonsoft.Json.Linq;

namespace ClipSharerWebsite
{
    public class VirtualFile
    {
        public readonly VirtualDirectory parent;
        public readonly string name;
        public readonly JObject fileInfo;

        public VirtualFile(VirtualDirectory parent, string name, JObject fileInfo)
        {
            this.parent = parent;
            this.name = name;
            this.fileInfo = fileInfo;
        }
    }
}
