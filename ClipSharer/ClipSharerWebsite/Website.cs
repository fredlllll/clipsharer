﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClipSharerWebsite
{
    public class Website : Template
    {
        public Website() : base("site")
        {

        }

        public string Render(string title, List<ItemInfo> items)
        {
            StringBuilder itemsMarkup = new StringBuilder();
            WebsiteItem wi = new WebsiteItem();
            foreach(var item in items)
            {
                itemsMarkup.AppendLine(wi.Render(item.name, item.thumb, item.link));
            }

            return Render(new List<Tuple<string, string>>()
            {
                new Tuple<string, string>("title",title),
                new Tuple<string, string>("items",itemsMarkup.ToString())
            });
        }
    }
}
