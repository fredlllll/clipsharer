﻿using System;
using System.Collections.Generic;

namespace ClipSharerWebsite
{
    public class WebsiteItem : Template
    {
        public readonly string thumb;
        public readonly string link;

        public WebsiteItem() : base("item")
        {

        }

        public string Render(string name, string thumb, string link)
        {
            return base.Render(new List<Tuple<string, string>>()
            {
                new Tuple<string, string>("name",name),
                new Tuple<string, string>("thumb",thumb),
                new Tuple<string, string>("link",link)
            });
        }
    }
}
