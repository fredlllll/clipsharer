﻿namespace ClipSharerWebsite
{
    public class ItemInfo
    {
        public readonly string name;
        public readonly string thumb;
        public readonly string link;

        public ItemInfo(string name, string thumb, string link)
        {
            this.name = name;
            this.thumb = thumb;
            this.link = link;
        }
    }
}
