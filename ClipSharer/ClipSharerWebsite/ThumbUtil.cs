﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace ClipSharerWebsite
{
    public static class ThumbUtil
    {
        /// <summary>
        /// creates smaller thumb next to <paramref name="file"/> with prefix <paramref name="thumbPrefix"/>.
        /// width of the smaller thumb will be <paramref name="width"/>
        /// </summary>
        /// <returns>the path to the resulting small thumb</returns>
        public static string CreateSmallThumb(string file, int width = 200, string thumbPrefix = "thumb_")
        {
            using(Bitmap src = new Bitmap(file))
            {
                double aspect = ((double)src.Height) / src.Width;
                int height = (int)(width * aspect);
                using(Bitmap dst = new Bitmap(width, height))
                {
                    using(Graphics g = Graphics.FromImage(dst))
                    {
                        g.DrawImage(src, 0, 0, width, height);
                        g.Flush();
                    }
                    string dstFile = Path.Combine(Path.GetDirectoryName(file), thumbPrefix + Path.GetFileName(file));
                    dst.Save(dstFile, ImageFormat.Jpeg);
                    return dstFile;
                }
            }
        }
    }
}
