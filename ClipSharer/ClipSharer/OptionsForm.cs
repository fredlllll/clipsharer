﻿using System.IO;
using System.Windows.Forms;
using ClipSharerShared;

namespace ClipSharer
{
    public partial class OptionsForm : HidableForm
    {
        public Options Options { get; set; } = new Options();

        string OptionsFile { get { return Path.Combine(Directories.ProgramDir, "options.json"); } }

        public OptionsForm()
        {
            InitializeComponent();
            Options.Load(OptionsFile);
            propertyGrid.SelectedObject = Options;

            FormClosing += OptionsForm_FormClosing;
        }

        private void OptionsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Options.Save(OptionsFile);
        }
    }
}
