﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClipSharerFFMPEG;
using ClipSharerShared;
using ClipSharerThumbnail;
using ClipSharerWebsite;
using NLog;

namespace ClipSharer
{
    public partial class MainForm : Form
    {
        private OptionsForm options;
        private ToolsForm tools;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private Task processingTask;
        private CancellationTokenSource processingCancellationTokenSource;

        public MainForm()
        {
            InitializeComponent();

            inputs.Add(btnStart);
            inputs.Add(btnStop);
            inputs.Add(btnOptions);
            inputs.Add(btnTools);
            inputs.Add(btnSearchFolder);
            inputs.Add(btnSearchWebsiteFolder);
            inputs.Add(txtFolder);
            inputs.Add(txtWebsiteFolder);

            options = new OptionsForm();
            options.Hide();
            tools = new ToolsForm();
            tools.Hide();

            Logging.AddTarget(new TextBoxTarget(txtOutput));

            FormClosing += MainForm_FormClosing;
            FormClosed += MainForm_FormClosed;
        }


        List<Control> inputs = new List<Control>();

        void ToggleInputs()
        {
            foreach(var c in inputs)
            {
                c.Enabled = !c.Enabled;
            }
        }

        bool VerifyFolders()
        {
            try
            {
                if(!Directory.Exists(txtFolder.Text))
                {
                    return false;
                }
                DirectoryInfo tmp = new DirectoryInfo(txtWebsiteFolder.Text);
            }
            catch
            {
                return false;
            }
            return true;
        }

        private async void BtnStart_Click(object sender, System.EventArgs e)
        {
            if(!VerifyFolders())
            {
                MessageBox.Show("Please check the folder paths for errors");
                return;
            }

            ToggleInputs();
            options.Hide();
            tools.Hide();

            txtOutput.Clear();

            processingCancellationTokenSource = new CancellationTokenSource();
            processingTask = new Task(ProcessFolder, processingCancellationTokenSource.Token);
            processingTask.Start();
            await processingTask;

            ToggleInputs();
        }

        void ProcessFolder()
        {
            try
            {
                CancellationToken ct = processingCancellationTokenSource.Token;

                object currentThreadLock = new object();
                RestartableThreadClass currentThread = null;
                ct.Register(() =>
                {
                    lock(currentThreadLock)
                    {
                        currentThread.Stop();
                    }
                });

                Stopwatch sw = new Stopwatch();
                sw.Start();

                DirectoryInfo folder = new DirectoryInfo(txtFolder.Text);
                DirectoryInfo clipSharerDirectory = new DirectoryInfo(Path.Combine(folder.FullName, ".ClipSharer"));
                DirectoryInfo websiteFolder = new DirectoryInfo(txtWebsiteFolder.Text);

                Options opt = options.Options;

                if(opt.Analyze)
                {
                    FolderAnalyzer fa = new FolderAnalyzer(folder, opt);
                    lock(currentThreadLock)
                    {
                        fa.Start();
                        currentThread = fa;
                    }
                    fa.Join();

                    ct.ThrowIfCancellationRequested();
                    GC.Collect();
                }

                if(opt.ExtractFrames)
                {
                    FrameExtractor fe = new FrameExtractor(clipSharerDirectory, opt);
                    lock(currentThreadLock)
                    {
                        fe.Start();
                        currentThread = fe;
                    }
                    fe.Join();

                    ct.ThrowIfCancellationRequested();
                    GC.Collect();
                }

                if(opt.GenerateThumbnails)
                {
                    ThumbnailGenerator gen = new ThumbnailGenerator(clipSharerDirectory, opt);
                    lock(currentThreadLock)
                    {
                        gen.Start();
                        currentThread = gen;
                    }
                    gen.Join();

                    ct.ThrowIfCancellationRequested();
                    GC.Collect();
                }

                if(opt.GenerateWebsite)
                {
                    WebsiteGenerator wgen = new WebsiteGenerator(clipSharerDirectory, websiteFolder, opt);
                    lock(currentThreadLock)
                    {
                        wgen.Start();
                        currentThread = wgen;
                    }
                    wgen.Join();

                    ct.ThrowIfCancellationRequested();
                    GC.Collect();
                }

                logger.Info("Processing done in " + sw.Elapsed);
            }
            catch(OperationCanceledException)
            {
                logger.Info("Processing was canceled");
                //end
            }
            GC.Collect();
            processingTask = null;
        }

        private void BtnSearchFolder_Click(object sender, System.EventArgs e)
        {
            fbd.SelectedPath = txtFolder.Text;
            if(fbd.ShowDialog() == DialogResult.OK)
            {
                txtFolder.Text = fbd.SelectedPath;
            }
        }

        private void BtnSearchWebsiteFolder_Click(object sender, System.EventArgs e)
        {
            fbd.SelectedPath = txtWebsiteFolder.Text;
            if(fbd.ShowDialog() == DialogResult.OK)
            {
                txtWebsiteFolder.Text = fbd.SelectedPath;
            }
        }


        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(processingTask != null)
            {
                if(MessageBox.Show("Do you want to cancel processing and exit ClipSharer?", "Processing still in progress", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    processingCancellationTokenSource.Cancel();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            options.ReallyClose();
        }

        private void BtnOptions_Click(object sender, System.EventArgs e)
        {
            options.Show();
        }

        private void BtnStop_Click(object sender, System.EventArgs e)
        {
            processingCancellationTokenSource.Cancel();
        }

        private void BtnTools_Click(object sender, EventArgs e)
        {
            tools.Show();
        }
    }
}
