﻿using System;
using System.Windows.Forms;
using NLog;
using NLog.Targets;

namespace ClipSharer
{
    public class TextBoxTarget : TargetWithLayout
    {
        private readonly TextBox textBox;

        public TextBoxTarget(TextBox textBox)
        {
            this.textBox = textBox;
        }

        protected override void Write(LogEventInfo logEvent)
        {
            string logMessage = Layout.Render(logEvent) + Environment.NewLine;

            textBox.BeginInvoke(new Action<string>((String text) =>
            {
                textBox.AppendText(text);
            }), new object[] { logMessage });
        }
    }
}
