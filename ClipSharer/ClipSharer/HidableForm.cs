﻿using System.Windows.Forms;

namespace ClipSharer
{
    public abstract partial class HidableForm : Form
    {
        protected bool preventClosing = true;

        public HidableForm()
        {
            InitializeComponent();

            FormClosing += HidableForm_FormClosing;
        }

        /// <summary>
        /// instead of hiding the form it really closes it
        /// </summary>
        public void ReallyClose()
        {
            preventClosing = false;
            Close();
        }

        private void HidableForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(preventClosing)
            {
                e.Cancel = true;
                Hide(); //only hide instead of closing
            }
        }
    }
}
